/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   push_swap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mkrutik <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/19 18:27:09 by mkrutik           #+#    #+#             */
/*   Updated: 2017/03/20 13:06:44 by mkrutik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_swap.h"

int		main(int argc, char **argv)
{
	t_array *new;

	if (argc == 1)
		return (0);
	new = (t_array*)malloc(sizeof(t_array));
	new->n_operation = 0;
	ft_create_array(new, argc, argv);
	ft_if_dublicate(new->a, new->n);
	if (ft_is_sort(new->a, new->in_a, new->n))
		return (0);
	if (new->n == 2)
	{
		ft_valid_operation(new, "sa");
		ft_putstr("sa\n");
		exit(0);
	}
	else if (new->n > 2)
		ft_alg(new);
	free(new->a);
	free(new);
	return (0);
}
