/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_alg.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mkrutik <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/19 17:44:32 by mkrutik           #+#    #+#             */
/*   Updated: 2017/03/20 12:45:14 by mkrutik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_swap.h"

void	ft_sort(t_array *src, t_operation *head)
{
	while (!ft_is_sort(src->a, src->in_a, src->in_a) && src->in_a <= 3)
	{
		if (src->a[0] > src->a[1])
		{
			if (src->in_a > 2 && src->a[0] < src->a[src->in_a - 1])
				ft_op(src, &head, "sa");
			else
				ft_op(src, &head, "ra");
		}
		else if (!ft_is_sort(src->a, src->in_a, src->in_a))
		{
			if (src->a[0] < src->a[1])
			{
				if (src->in_a > 2 && src->a[0] > src->a[src->in_a - 1])
					ft_op(src, &head, "rra");
				else
					ft_op(src, &head, "ra");
			}
		}
	}
}

void	ft_swap_from_a(t_array *s, t_operation *op, int len, int i)
{
	int		med;
	int		rot;
	int		push;

	push = 0;
	rot = 0;
	(len <= 3) ? (ft_sort_new(s, op, 1)) : 0;
	if (len <= 3)
		return ;
	med = ft_find_med(s->a, len, 1);
	while (ft_is_lower_or_bigger_then_med(s->a, len - i, med, 1) && i++ < len)
	{
		(s->a[0] < med) ? (push++) : (rot++);
		(s->a[0] < med) ? (ft_op(s, &op, "pb")) : (ft_op(s, &op, "ra"));
	}
	if (rot > (s->in_a / 2) && s->in_a > 3)
		while (rot++ < s->in_a)
			ft_op(s, &op, "ra");
	else if (s->in_a > 3)
		while (rot--)
			ft_op(s, &op, "rra");
	ft_swap_from_a(s, op, (len - push), 0);
	ft_swap_from_b(s, op, push, 0);
	while (push--)
		ft_op(s, &op, "pa");
}

void	ft_swap_from_b(t_array *src, t_operation *op, int len, int i)
{
	int		med;
	int		rot;
	int		push;

	push = 0;
	rot = 0;
	(len <= 3) ? (ft_sort_new(src, op, 0)) : 0;
	if (len <= 3)
		return ;
	med = ft_find_med(src->b, len, 0);
	while (ft_is_lower_or_bigger_then_med(src->b, len - i, med, 0) && i++ < len)
	{
		(src->b[0] > med) ? (push++) : (rot++);
		(src->b[0] > med) ? (ft_op(src, &op, "pa")) : (ft_op(src, &op, "rb"));
	}
	ft_swap_from_a(src, op, push, 0);
	if (rot > (src->in_b / 2) && src->in_b > 3)
		while (rot++ < src->in_b)
			ft_op(src, &op, "rb");
	else if (src->in_b > 3)
		while (rot--)
			ft_op(src, &op, "rrb");
	ft_swap_from_b(src, op, (len - push), 0);
	while (push--)
		ft_op(src, &op, "pb");
}

void	ft_alg(t_array *src)
{
	t_operation		*head;
	t_operation		*point;

	head = (t_operation*)malloc(sizeof(t_operation));
	head->s = NULL;
	head->next = NULL;
	src->n_operation = 0;
	if (src->in_a > 3)
		ft_swap_from_a(src, head, src->in_a, 0);
	else
		ft_sort(src, head);
	ft_operation_optimize(head, point, 1, 0);
	ft_print_and_free(head, src);
}
