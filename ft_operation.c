/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_operation.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mkrutik <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/19 18:26:28 by mkrutik           #+#    #+#             */
/*   Updated: 2017/03/19 18:26:31 by mkrutik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_swap.h"

void	ft_shift_down(int *src, int n)
{
	int		i;
	int		j;
	int		tmp;

	if (n <= 1)
		return ;
	i = 0;
	j = n - 1;
	tmp = src[j];
	while (i <= j)
	{
		src[j] = (j == 0) ? tmp : src[j - 1];
		j--;
	}
}

void	ft_shift_up(int *src, int n)
{
	int		i;
	int		tmp;

	if (n <= 1)
		return ;
	i = 0;
	tmp = src[0];
	while (i < n)
	{
		src[i] = (i + 1 == n) ? tmp : src[i + 1];
		i++;
	}
}

void	ft_swap_two_first(t_array *src, int f)
{
	int		*tmp;
	int		t;

	tmp = NULL;
	(f == 1 && src->in_a > 1) ? (tmp = src->a) : 0;
	(f == 2 && src->in_b > 1) ? (tmp = src->b) : 0;
	if (tmp != NULL)
	{
		t = tmp[0];
		tmp[0] = tmp[1];
		tmp[1] = t;
		(f == 1) ? (src->a = tmp) : 0;
		(f != 1) ? (src->b = tmp) : 0;
	}
}

void	ft_copy_i_a(int **t, int *src, int n)
{
	int		i;
	int		*point;

	if (n != 0)
	{
		i = 0;
		point = *t;
		while (i < n)
		{
			point[i] = src[i];
			i++;
		}
		free(src);
		*t = point;
	}
}

void	ft_valid_operation(t_array *s, char *oper)
{
	int f;

	f = 0;
	(ft_strcmp(oper, "sa") == 0) ? ft_swap_two_first(s, 1) : 0;
	(ft_strcmp(oper, "sa") == 0 || ft_strcmp(oper, "sb") == 0) ? (f++) : 0;
	(ft_strcmp(oper, "ss") == 0 || ft_strcmp(oper, "pa") == 0) ? (f++) : 0;
	(ft_strcmp(oper, "pb") == 0 || ft_strcmp(oper, "ra") == 0) ? (f++) : 0;
	(ft_strcmp(oper, "rb") == 0 || ft_strcmp(oper, "rr") == 0) ? (f++) : 0;
	(ft_strcmp(oper, "rra") == 0 || ft_strcmp(oper, "rrb") == 0) ? (f++) : 0;
	(ft_strcmp(oper, "rrr") == 0) ? (f++) : 0;
	(ft_strcmp(oper, "sb") == 0) ? ft_swap_two_first(s, 2) : 0;
	(ft_strcmp(oper, "ss") == 0) ? ft_swap_two_first(s, 1) : 0;
	(ft_strcmp(oper, "ss") == 0) ? ft_swap_two_first(s, 2) : 0;
	(ft_strcmp(oper, "pa") == 0) ? ft_put(s, 1) : 0;
	(ft_strcmp(oper, "pb") == 0) ? ft_put(s, 2) : 0;
	(ft_strcmp(oper, "ra") == 0) ? ft_shift_up(s->a, s->in_a) : 0;
	(ft_strcmp(oper, "rb") == 0) ? ft_shift_up(s->b, s->in_b) : 0;
	(ft_strcmp(oper, "rr") == 0) ? ft_shift_up(s->a, s->in_a) : 0;
	(ft_strcmp(oper, "rr") == 0) ? ft_shift_up(s->b, s->in_b) : 0;
	(ft_strcmp(oper, "rra") == 0) ? ft_shift_down(s->a, s->in_a) : 0;
	(ft_strcmp(oper, "rrb") == 0) ? ft_shift_down(s->b, s->in_b) : 0;
	(ft_strcmp(oper, "rrr") == 0) ? ft_shift_down(s->a, s->in_a) : 0;
	(ft_strcmp(oper, "rrr") == 0) ? ft_shift_down(s->b, s->in_b) : 0;
	(f != 1) ? (ft_error(3)) : 0;
}
