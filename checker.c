/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   checker.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mkrutik <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/19 17:43:11 by mkrutik           #+#    #+#             */
/*   Updated: 2017/03/20 13:08:20 by mkrutik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_swap.h"

int		main(int argc, char **argv)
{
	t_array		*new;
	char		*src;

	(argc == 1) ? (ft_error(1)) : 0;
	(argv[1][0] == '\0') ? (ft_error(1)) : 0;
	new = (t_array*)malloc(sizeof(t_array));
	ft_create_array(new, argc, argv);
	ft_if_dublicate(new->a, new->n);
	src = NULL;
	new->n_operation = 0;
	while (get_next_line(0, &src) > 0)
	{
		ft_valid_operation(new, src);
		(new->print_stack == 1) ? (ft_putstr("          \e[4;35m")) : 0;
		(new->print_stack == 1) ? ft_putstr(src) : 0;
		(new->print_stack == 1) ? (ft_print_stack(new, -1, 0, 0)) : 0;
		free(src);
		new->n_operation++;
	}
	(new->p_n_oper == 1) ? (ft_putstr("\x1b[36mNumbers operations = ")) : 0;
	(new->p_n_oper == 1) ? (ft_putnbr(new->n_operation)) : 0;
	(new->p_n_oper == 1) ? (ft_putstr("\n")) : 0;
	(ft_is_sort(new->a, new->in_a, new->n) == 0) ?
	(ft_putstr("\033[0;31mKO\n\e[0m")) : (ft_putstr("\033[0;32mOK\n\e[0m"));
	return (0);
}
