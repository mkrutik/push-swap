/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_operation_2.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mkrutik <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/19 18:31:31 by mkrutik           #+#    #+#             */
/*   Updated: 2017/03/19 18:31:34 by mkrutik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_swap.h"

void	ft_put(t_array *src, int f)
{
	int	*res;

	(f == 1 && src->in_b > 0) ? (ft_put_a(src)) : 0;
	if (f == 2 && src->in_a > 0)
	{
		res = (int*)malloc(sizeof(int) * (src->in_b + 1));
		ft_copy_i_a(&res, src->b, src->in_b);
		src->b = res;
		ft_shift_down(src->b, src->in_b + 1);
		src->b[0] = src->a[0];
		ft_shift_up(src->a, src->in_a);
		src->in_a--;
		src->in_b++;
		if (src->in_a != 0)
		{
			res = (int*)malloc(sizeof(int) * src->in_a);
			ft_copy_i_a(&res, src->a, src->in_a);
			src->a = res;
		}
		else
			free(src->a);
	}
}

void	ft_put_a(t_array *src)
{
	int		*res;

	res = (int*)malloc(sizeof(int) * (src->in_a + 1));
	ft_copy_i_a(&res, src->a, src->in_a);
	src->a = res;
	ft_shift_down(src->a, (src->in_a + 1));
	src->a[0] = src->b[0];
	ft_shift_up(src->b, src->in_b);
	src->in_b--;
	src->in_a++;
	if (src->in_b != 0)
	{
		res = (int*)malloc(sizeof(int) * src->in_b);
		ft_copy_i_a(&res, src->b, src->in_b);
		src->b = res;
	}
	else
		free(src->b);
}
