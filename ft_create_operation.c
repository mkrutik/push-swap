/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_create_operation.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mkrutik <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/19 17:48:56 by mkrutik           #+#    #+#             */
/*   Updated: 2017/03/20 12:15:28 by mkrutik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_swap.h"

void	ft_op(t_array *s, t_operation **head, char *src)
{
	t_operation		*new;
	t_operation		*point;

	point = *head;
	ft_valid_operation(s, src);
	if (point->s == NULL)
		point->s = ft_strdup(src);
	else
	{
		while (point->next != NULL)
			point = point->next;
		new = (t_operation*)malloc(sizeof(t_operation));
		new->s = ft_strdup(src);
		new->next = NULL;
		point->next = new;
	}
	*head = point;
}

int		ft_optimization(t_operation *h, int f)
{
	if (h->next == NULL)
		return (0);
	(h->s[0] == 's' && h->next->s[0] == 's' && ((h->s[1] == 'a' &&
		h->next->s[1] == 'b') || (h->s[1] == 'b' && h->next->s[1] == 'a')))
			? (f = 1) : 0;
	(h->s[0] == 'r' && h->next->s[0] == 'r' && ((h->s[1] == 'a' &&
		h->next->s[1] == 'b') || (h->s[1] == 'b' && h->next->s[1] == 'a')))
			? (f = 2) : 0;
	(h->s[0] == 'r' && h->next->s[0] == 'r' && ((h->s[2] == 'a' &&
		h->next->s[2] == 'b') || (h->s[2] == 'b' && h->next->s[2] == 'a')))
			? (f = 3) : 0;
	(h->s[0] == 'p' && h->next->s[0] == 'p' && ((h->s[1] == 'a' &&
		h->next->s[1] == 'b') || (h->s[1] == 'b' && h->next->s[1] == 'a')))
			? (f = 4) : 0;
	(h->s[0] == 'r' && h->next->s[0] == 'r' && ((h->s[1] == 'a' &&
		h->next->s[1] == 'r' && h->next->s[2] == 'a') || (h->s[1] == 'r' &&
			h->s[2] == 'a' && h->next->s[1] == 'a'))) ? (f = 5) : 0;
	(h->s[0] == 'r' && h->next->s[0] == 'r' && ((h->s[1] == 'b' &&
		h->next->s[1] == 'r' && h->next->s[2] == 'b') || (h->s[1] == 'r' &&
			h->s[2] == 'b' && h->next->s[1] == 'b'))) ? (f = 6) : 0;
	(f == 1 || f == 2 || f == 3) ? (ft_strdel(&h->s)) : 0;
	(f == 1) ? (h->s = ft_strdup("ss")) : 0;
	(f == 2) ? (h->s = ft_strdup("rr")) : 0;
	(f == 3) ? (h->s = ft_strdup("rrr")) : 0;
	return (f);
}

void	ft_print_and_free(t_operation *src, t_array *s)
{
	t_operation *del;

	while (src)
	{
		del = src;
		ft_putstr(src->s);
		ft_putstr("\n");
		src = src->next;
		ft_strdel(&del->s);
		free(del);
		s->n_operation++;
	}
}

void	ft_operation_optimize(t_operation *head, t_operation *del, int f, int x)
{
	t_operation	*point;

	while (f)
	{
		point = head;
		f = 0;
		del = point;
		while (point != NULL)
		{
			if ((x = ft_optimization(point, 0)) != 0)
			{
				f = 1;
				(x == 4 || x == 5 || x == 6) ? (del->next = point->next->next)
					: (del = point->next);
				(x == 4 || x == 5 || x == 6) ? (free(point->next->s)) :
					(point->next = point->next->next);
				(x == 4 || x == 5 || x == 6) ? (free(point->s)) : (free(del));
				(x == 4 || x == 5 || x == 6) ? (free(point->next)) : 0;
				(x == 4 || x == 5 || x == 6) ? (free(point)) : 0;
				(x == 4 || x == 5 || x == 6) ? (point = del) : 0;
			}
			del = point;
			point = point->next;
		}
	}
}
