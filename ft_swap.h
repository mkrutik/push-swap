/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_swap.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mkrutik <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/15 11:20:16 by mkrutik           #+#    #+#             */
/*   Updated: 2017/03/20 14:25:53 by mkrutik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_SWAP_H
# define FT_SWAP_H

# include "libft/libft.h"
# include "get_next_line.h"
# include <limits.h>
# include <stdio.h>
# define MY_ABS(x) (((x) < 0) ? -(x) : (x))

typedef struct			s_array
{
	int					*a;
	int					*b;
	int					n;
	int					in_a;
	int					in_b;
	int					p_n_oper;
	int					print_stack;
	unsigned int		n_operation;
}						t_array;

typedef struct			s_operation
{
	char				*s;
	struct s_operation	*next;
}						t_operation;

void					ft_create_array(t_array *src, int argc, char **argv);
void					ft_if_dublicate(int *array, int n);
int						ft_is_sort(int *array, int in_a, int n);
void					ft_error(int n);
void					ft_valid_operation(t_array *s, char *oper);
void					ft_alg(t_array *src);
void					quicksort(int *list, int low, int high, int i);
void					ft_op(t_array *s, t_operation **head, char *src);
void					ft_operation_optimize(t_operation *head, t_operation *d,
						int f, int x);
void					ft_print_and_free(t_operation *src, t_array *s);
int						ft_is_lower_or_bigger_then_med(int *src, int n, int med,
						int f);
int						ft_find_med(int *src, int n, int f);
void					ft_swap_from_b(t_array *src, t_operation *op, int len,
						int i);
void					ft_sort_new(t_array *s, t_operation *op, int f);
void					ft_put(t_array *src, int f);
void					ft_put_a(t_array *src);
void					ft_shift_down(int *src, int n);
void					ft_shift_up(int *src, int n);
void					ft_swap_two_first(t_array *src, int f);
void					ft_valid_operation(t_array *s, char *oper);
void					ft_copy_i_a(int **t, int *src, int n);
void					ft_print_stack(t_array *src, int i, int len, int l);
#endif
