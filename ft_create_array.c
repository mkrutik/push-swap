/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_create_array.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mkrutik <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/19 17:47:40 by mkrutik           #+#    #+#             */
/*   Updated: 2017/03/20 10:50:55 by mkrutik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_swap.h"

int		ft_atoi_ssize(char *src)
{
	ssize_t		tmp;
	ssize_t		sign;

	tmp = 0;
	sign = 1;
	while (ft_isspace(*src))
		src++;
	if (*src == '-')
		sign = -1;
	if (*src == '-' || *src == '+')
		src++;
	while (ft_isdigit(*src))
	{
		tmp *= 10;
		tmp += *src - '0';
		src++;
	}
	if (*src != '\0' && *src != '-' && !ft_isdigit(*src))
		ft_error(2);
	tmp = tmp * sign;
	if (tmp > 2147483647 || tmp < -2147483648)
		ft_error(2);
	return ((int)tmp);
}

int		ft_find_flag(t_array *src, char *argv)
{
	int	i;

	i = 1;
	src->p_n_oper = 0;
	src->print_stack = 0;
	if (argv[0] == '-')
		while (i < ft_strlen(argv))
		{
			if (argv[i] == 'n')
				src->p_n_oper = 1;
			else if (argv[i] == 'v')
				src->print_stack = 1;
			else
				return (0);
			i++;
		}
	else
		return (0);
	return (1);
}

void	ft_create_array(t_array *src, int argc, char **argv)
{
	int		i;
	int		j;
	int		f;

	src->a = (int*)malloc(sizeof(int) * (argc - 1));
	f = ft_find_flag(src, argv[1]);
	src->in_a = (f == 0) ? (argc - 1) : (argc - 2);
	src->in_b = 0;
	src->n = src->in_a;
	i = (f == 0) ? 1 : 2;
	j = 0;
	while (i < argc)
	{
		src->a[j] = ft_atoi_ssize(argv[i]);
		i++;
		j++;
	}
}
