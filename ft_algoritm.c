/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_algoritm.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mkrutik <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/19 18:30:53 by mkrutik           #+#    #+#             */
/*   Updated: 2017/03/20 12:36:21 by mkrutik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_swap.h"

int		*ft_copy(int *src, int n)
{
	int		*res;
	int		i;

	res = (int*)malloc(sizeof(int) * n);
	i = 0;
	while (i < n)
	{
		res[i] = src[i];
		i++;
	}
	return (res);
}

void	ft_res_sort(int *src, int n)
{
	int		i;
	int		tmp;

	i = 0;
	tmp = 0;
	while (i < n)
	{
		tmp = src[i];
		src[i] = src[n - 1];
		src[n - 1] = tmp;
		i++;
		n--;
	}
}

void	ft_sort_new(t_array *s, t_operation *op, int f)
{
	if (f == 1)
	{
		(s->in_a > 1 && s->a[0] > s->a[1]) ? (ft_op(s, &op, "sa")) : 0;
		if (s->in_a > 2 && s->a[1] > s->a[2])
		{
			ft_op(s, &op, "ra");
			ft_op(s, &op, "sa");
			ft_op(s, &op, "rra");
		}
		(s->in_a > 1 && s->a[0] > s->a[1]) ? (ft_op(s, &op, "sa")) : 0;
	}
	else
	{
		(s->in_b > 1 && s->b[0] < s->b[1]) ? (ft_op(s, &op, "sb")) : 0;
		if (s->in_b > 2 && s->b[1] < s->b[2])
		{
			ft_op(s, &op, "rb");
			ft_op(s, &op, "sb");
			ft_op(s, &op, "rrb");
		}
		(s->in_b > 1 && s->b[0] < s->b[1]) ? (ft_op(s, &op, "sb")) : 0;
	}
}

int		ft_find_med(int *src, int n, int f)
{
	int		res;
	int		*tmp;

	tmp = ft_copy(src, n);
	quicksort(tmp, 0, n - 1, 0);
	if (f != 1)
		ft_res_sort(tmp, n);
	res = (tmp[n / 2]);
	free(tmp);
	return (res);
}

int		ft_is_lower_or_bigger_then_med(int *src, int n, int med, int f)
{
	int		i;

	i = 0;
	while (i < n)
	{
		if (f == 1 && src[i] < med)
			return (1);
		if (f == 0 && src[i] > med)
			return (1);
		i++;
	}
	return (0);
}
