# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: mkrutik <marvin@42.fr>                     +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2017/03/14 18:53:41 by mkrutik           #+#    #+#              #
#    Updated: 2017/03/20 14:25:32 by mkrutik          ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME_1 = push_swap
NAME_2 = checker

SRC = ft_error.c \
	  ft_operation.c \
	  ft_alg.c \
	  ft_algoritm.c \
	  ft_create_operation.c \
	  ft_operation_2.c \
	  ft_create_array.c \
	  get_next_line.c \
	  ft_bonus.c

OBJ = $(SRC:.c=.o)

HEAD = -I ft_swap.h get_next_line.h

GFLAGS = -Wall -Wextra -Werror

LIBINC = -I libft/libft.h -L./libft -lft

all: $(NAME_1) $(NAME_2)

$(NAME_1): $(OBJ)
	make -C libft/
	gcc $(GFLAGS) $(OBJ) push_swap.c -o $(NAME_1) $(LIBINC)

$(NAME_2):
	gcc $(GFLAGS) $(OBJ) checker.c -o $(NAME_2) $(LIBINC)

clean:
	rm -f $(OBJ) push_swap.o checker.o
	make clean -C libft/

fclean: clean
	rm -f $(NAME_1) $(NAME_2)
	make fclean -C libft/

re: fclean all
