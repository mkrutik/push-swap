/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_bonus.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mkrutik <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/19 18:06:25 by mkrutik           #+#    #+#             */
/*   Updated: 2017/03/20 14:21:43 by mkrutik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_swap.h"

void	ft_sleep(void)
{
	size_t	i;

	i = 0;
	while (i < 1000000000)
		i++;
	return ;
}

int		ft_int_strlen(int num)
{
	int				len;
	unsigned int	tmp;

	len = 1;
	if (num < 0)
	{
		len = 2;
		tmp = -num;
	}
	else
		tmp = num;
	while (tmp >= 10)
	{
		len++;
		tmp /= 10;
	}
	return (len);
}

void	ft_print_stack(t_array *src, int i, int len, int l)
{
	ft_putstr("\n\x1b[32m__Stack_A__|__Stack_B__\x1b[0m\n");
	len = (src->in_a > src->in_b) ? src->in_a : src->in_b;
	while (++i < len)
	{
		ft_putstr("\x1b[30m\x1b[47m");
		l = (i < src->in_a) ? (11 - ft_int_strlen(src->a[i])) : 0;
		while (l--)
			ft_putstr(" ");
		(i < src->in_a) ? (ft_putnbr(src->a[i])) : 0;
		(i >= src->in_a) ? (ft_putstr("           ")) : 0;
		ft_putstr("\x1b[0m\x1b[32m|\x1b[0m\x1b[37m\x1b[40m");
		l = (i < src->in_b) ? (11 - ft_int_strlen(src->b[i])) : 0;
		while (l--)
			ft_putstr(" ");
		(i < src->in_b) ? (ft_putnbr(src->b[i])) : 0;
		(i >= src->in_b) ? (ft_putstr("           ")) : 0;
		ft_putstr("\x1b[0m\n");
	}
	ft_putstr("\x1b[0m\n");
	ft_sleep();
}
