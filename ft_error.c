/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_error.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mkrutik <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/15 11:33:44 by mkrutik           #+#    #+#             */
/*   Updated: 2017/03/20 12:35:17 by mkrutik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_swap.h"

void	ft_error(int n)
{
	ft_putstr("\033[0;31m");
	ft_putstr("ERROR\n");
	(n == 1) ? (ft_putstr("WRONG NUMBER OF ARGUMENTS\n")) : 0;
	(n == 2) ? (ft_putstr("INVALID ARRAY\n")) : 0;
	(n == 3) ? (ft_putstr("THIS COMMAND ISN`T SUPPORTED\n")) : 0;
	(n == 4) ? (ft_putstr("ARRAY HAS DUBLICATE\n")) : 0;
	ft_putstr("USAGE: ./checker <int array separated by space>\n");
	exit(-1);
}

int		ft_is_sort(int *array, int in_a, int n)
{
	int	i;

	i = 0;
	if (in_a < n)
		return (0);
	if (in_a == n && n == 1)
		return (1);
	while (i < (n - 1))
	{
		if (array[i] > array[i + 1])
			return (0);
		i++;
	}
	return (1);
}

void	ft_if_dublicate(int *array, int n)
{
	int		i;
	int		tmp;
	int		j;

	i = 0;
	while (i < n)
	{
		tmp = array[i];
		j = i + 1;
		while (j < n)
		{
			if (array[j] == tmp)
				ft_error(4);
			j++;
		}
		++i;
	}
}

void	quicksort(int *list, int low, int high, int i)
{
	int		pivot;
	int		j;
	int		temp;

	if (low < high)
	{
		pivot = low;
		i = low;
		j = high;
		while (i < j)
		{
			while (list[i] <= list[pivot] && i <= high)
				i++;
			while (list[j] > list[pivot] && j >= low)
				j--;
			(i < j) ? (temp = list[i]) : 0;
			(i < j) ? (list[i] = list[j]) : 0;
			(i < j) ? (list[j] = temp) : 0;
		}
		temp = list[j];
		list[j] = list[pivot];
		list[pivot] = temp;
		quicksort(list, low, j - 1, 0);
		quicksort(list, j + 1, high, 0);
	}
}
